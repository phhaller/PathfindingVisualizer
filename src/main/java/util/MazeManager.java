package util;

public class MazeManager {

  private String[][] map;


  public MazeManager(int width, int height) {
    map = new String[2*width][2*height+1];

    initFreshMaze();
    breakWall(0, width, 0, height);

    for (int x = 0; x < 2*width; x++) {
      for (int y = 0; y < 2*height+1; y++) {
        if (x == 2*width - 1) {
          map[x][y] = "obstacle";
        }
      }
    }

  }


  // Init the maze creation
  private void initFreshMaze() {
    for (int r = 0; r < map.length; r++) {
      for (int c = 0; c < map[0].length; c++) {
        if (r%2 == 1 && c%2 == 1) {
          map[r][c] = "none";
        } else {
          map[r][c] = "obstacle";
        }
      }
    }
  }


  // Delete walls to create the maze
  private void breakWall( int ra, int rb, int ca, int cb) {
    if (cb-ca <= 1 && rb-ra <= 1) return;
    if (rb-ra >= cb-ca) {
      int midR = (ra+rb)/2;
      int offC = (int)(Math.random()*(cb-ca))+ca;
      map[midR*2][offC*2+1] = "none";
      breakWall(ra, midR, ca, cb);
      breakWall(midR, rb, ca, cb);
    } else {
      int midC = (ca+cb)/2;
      int offR = (int)(Math.random()*(rb-ra))+ra;
      map[offR*2+1][midC*2] = "none";
      breakWall(ra, rb, ca, midC);
      breakWall(ra, rb, midC, cb);
    }
  }


  public String[][] getMap() {
    return map;
  }

}

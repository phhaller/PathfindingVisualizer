package util;

import java.io.InputStream;
import java.util.Scanner;

public class MapReader {

  String[][] csvMap = null;

  public MapReader(String filename) {
    readCSVFile(filename);
  }


  // Read a csv file by its filename
  private void readCSVFile(String filename) {

    int counter = 0;
    int height = 0;
    int width = 0;

    InputStream inputStream = getClass().getResourceAsStream("/csv/" + filename);
    Scanner s = new Scanner(inputStream);
    while (s.hasNextLine()) {

      String col = s.nextLine();
      char[] entries = col.toCharArray();

      if (col.startsWith("height")) {
        height = Integer.parseInt(col.substring(7));
      }

      if (col.startsWith("width")) {
        width = Integer.parseInt(col.substring(6));
      }

      if (csvMap == null && width > 0 && height > 0) {
        csvMap = new String[width][height];
      }

      if (counter > 3) {

        for (int i = 0; i < entries.length; i++) {

          csvMap[i][counter - 4] = getTileType(entries[i]);

        }

      }


      counter += 1;

    }

  }


  // Get the tile type based on the ones used in the Dragon Age: Origin maps
  private String getTileType(char c) {

    String tileType = "";

    if (c == '.') {
      tileType = "ground";
    }

    if (c == 'T' || c == '@') {
      tileType = "obstacle";
    }

    return tileType;

  }


  public String[][] getCsvMap() {
    return csvMap;
  }

}

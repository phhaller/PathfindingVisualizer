package util;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Util {

  private static int counter = 0;

  // Get the neighbours of a node according to the diagonal strategy.
  public static ArrayList<Node> getNeighbours(Node[][] nodes, Node node, String diagonal) {

    ArrayList<Node> neighbours = new ArrayList<>();
    int x = node.getX();
    int y = node.getY();

    boolean n = false, s = false, e = false, w = false, ne = false, nw = false, se = false, sw = false;

    if (isWalkable(nodes, x, y - 1)) {
      neighbours.add(nodes[x][y - 1]);
      n = true;
    }

    if (isWalkable(nodes, x + 1, y)) {
      neighbours.add(nodes[x + 1][y]);
      e = true;
    }

    if (isWalkable(nodes, x, y + 1)) {
      neighbours.add(nodes[x][y+1]);
      s = true;
    }

    if (isWalkable(nodes, x - 1, y)) {
      neighbours.add(nodes[x-1][y]);
      w = true;
    }

    switch (diagonal) {
      case "NEVER":
        return neighbours;
      case "NO_OBSTACLES":
        ne = n && e;
        nw = n && w;
        se = s && e;
        sw = s && w;
        break;
      case "ONE_OBSTACLE":
        ne = n || e;
        nw = n || w;
        se = s || e;
        sw = s || w;
        break;
      case "ALWAYS":
        ne = nw = se = sw = true;
    }

    if (nw && isWalkable(nodes, x - 1, y - 1)) {
      neighbours.add(nodes[x - 1][y - 1]);
    }

    if (ne && isWalkable(nodes,  x + 1, y - 1)) {
      neighbours.add(nodes[x + 1][y - 1]);
    }

    if (se && isWalkable(nodes, x + 1, y + 1)) {
      neighbours.add(nodes[x + 1][y + 1]);
    }

    if (sw && isWalkable(nodes, x - 1, y + 1)) {
      neighbours.add(nodes[x - 1][y + 1]);
    }

    return neighbours;

  }


  // Check if a node is walkable or not.
  private static boolean isWalkable(Node[][] nodes, int x, int y) {
    return x >= 0 && y >= 0 && x < nodes.length && y < nodes[0].length && !nodes[x][y].getType().equals("obstacle");
  }


  // Get the distance between two nodes according to the set heuristics
  public static double getDistance(Node a, Node b, String heuristics) {

    int dx = Math.abs(a.getX() - b.getX());
    int dy = Math.abs(a.getY() - b.getY());

    switch (heuristics) {
      case "manhattan":
        return dx + dy;
      case "euclidean":
        return (Math.sqrt(dx * dx + dy * dy));
      case "octile":
        double F = Math.sqrt(2.0) - 1;
        return ((dx < dy) ? F * dx + dy : F * dy + dx);
      case "chebyshev":
        return Math.max(dx, dy);
      default:
        return dx + dy;
    }

  }


  // Animate the coloring of nodes that are being added to the open list. This colors all nodes equally and does not
  // distinguish between nodes in the open list and nodes in the closed list.
  public static void animateNodeColoring(Node node, Node start, Node end, int speed) {

    if (node != start && node != end) {

      Timeline timeline = new Timeline(

          new KeyFrame(Duration.millis((10 - (speed - 1))), new KeyValue(node.getNode().fillProperty(), Color.web("#4E4642"))),
          new KeyFrame(Duration.millis(50 + (10 - (speed - 1)) * 3), new KeyValue(node.getNode().fillProperty(), Color.web("#735E50"))),
          new KeyFrame(Duration.millis(100 + (10 - (speed - 1)) * 3), new KeyValue(node.getNode().fillProperty(), Color.web("#A6927A"))),
          new KeyFrame(Duration.millis(150 + (10 - (speed - 1)) * 3), new KeyValue(node.getNode().fillProperty(), Color.web("#C3BDB1"))),
          new KeyFrame(Duration.millis(200 + (10 - (speed - 1)) * 3), new KeyValue(node.getNode().fillProperty(), Color.web("#9EC0C0")))

          );

      timeline.setCycleCount(1);
      timeline.play();

    }

  }


  // Construct the path, draw it, and create a result window.
  public static void constructPath(Node node, Node start, Node end, AnchorPane background, String algorithm,
                                   String heuristics, String diagonal, int speed, float duration, List<Node> openList,
                                   List<Node> closedList) {
    LinkedList<Node> path = new LinkedList<>();
    while (node.getParent() != null) {
      path.addFirst(node);
      node = node.getParent();
    }

    drawPath(path, start, end);

    new ResultManager(background, algorithm, heuristics, diagonal, String.valueOf(speed),
        String.valueOf(duration), String.valueOf(openList.size()), String.valueOf(closedList.size()));

  }


  // In case of JPS and JPS+, the path between the individual jump points has to be retraced. Same as the construct path
  // function, it will construct the path, draw it, and create a result window.
  public static void retracePath(Node[][] nodes, Node node, Node start, Node end, AnchorPane background,
                                 String algorithm, String heuristics, String diagonal, int speed, float duration,
                                 List<Node> openList, List<Node> closedList) {
    LinkedList<Node> path = new LinkedList<>();
    Node current;
    Node parent;
    while (node.getParent() != null) {
      current = node;
      parent = node.getParent();

      path.addFirst(node);

      if (parent.getX() == current.getX()) {
        int dist = parent.getY() - current.getY();
        int sign = dist > 0 ? 1 : -1;
        dist = Math.abs(dist);
        for (int i = 0; i < dist; i++) {
          path.addFirst(nodes[current.getX()][current.getY() + i * sign]);
        }
      } else if (parent.getY() == current.getY()) {
        int dist = parent.getX() - current.getX();
        int sign = dist > 0 ? 1 : -1;
        dist = Math.abs(dist);
        for (int i = 0; i < dist; i++) {
          path.addFirst(nodes[current.getX() + i * sign][current.getY()]);
        }
      } else {

        int distX = parent.getX() - current.getX();
        int signX = distX > 0 ? 1 : -1;
        distX = Math.abs(distX);

        int distY = parent.getY() - current.getY();
        int signY = distY > 0 ? 1 : -1;

        for (int i = 0; i < distX; i++) {
          path.addFirst(nodes[current.getX() + i * signX][current.getY() + i * signY]);
        }

      }

      node = parent;
    }

    drawPath(path, start, end);

    new ResultManager(background, algorithm, heuristics, diagonal, String.valueOf(speed),
        String.valueOf(duration), String.valueOf(openList.size()), String.valueOf(closedList.size()));

  }


  //
  private static void drawPath(LinkedList<Node> path, Node start, Node end) {

    // Outer timeline is needed, as the node coloring animation otherwise will overwrite the path drawing
    // when the animation speed is max and path distance is short.
    Timeline delayTimeline = new Timeline(new KeyFrame(Duration.millis(150)));
    delayTimeline.setCycleCount(1);
    delayTimeline.play();

    delayTimeline.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(7), new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event) {

            if (counter < path.size()) {
              Node n = path.get(counter);
              if (n != start && n != end) {
                n.getNode().getStyleClass().add("path");
              }
              counter++;
            }

          }
        }));

        timeline.setCycleCount(path.size());
        timeline.play();

        timeline.setOnFinished(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event) {
            counter = 0;
          }
        });

      }
    });

  }

}

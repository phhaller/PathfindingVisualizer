package util;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class HowToManager {

  private AnchorPane background;
  private ScrollPane scrollPane;
  private AnchorPane contentPane;
  private AnchorPane howToPane;
  private Label closeLabel;

  public HowToManager(AnchorPane background) {

    this.background = background;
    createHowTo();
    createHandlers();

  }


  // Create the How To window
  private void createHowTo() {

    howToPane = new AnchorPane();
    howToPane.setPrefSize(1000, 600);
    howToPane.setTranslateX(100);
    howToPane.setTranslateY(130);
    howToPane.getStyleClass().add("howToPane");
    howToPane.setVisible(false);

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/howToWindow.fxml"));
      contentPane = loader.load();
    } catch (IOException e) {
      e.printStackTrace();
    }

    scrollPane = new ScrollPane();
    scrollPane.setContent(contentPane);
    scrollPane.setTranslateX(100);
    scrollPane.setTranslateY(50);
    scrollPane.setPrefViewportWidth(800);
    scrollPane.setPrefViewportHeight(500);
    scrollPane.hbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
    scrollPane.getStyleClass().add("scrollPane");


    closeLabel = new Label();
    closeLabel.setTranslateX(20);
    closeLabel.setTranslateY(20);
    closeLabel.setGraphic(new ImageView(new Image("/pictures/closeIconLarge.png")));
    closeLabel.getStyleClass().add("closeLabel");

    howToPane.getChildren().addAll(closeLabel, scrollPane);

    background.getChildren().add(howToPane);

  }


  // Create the handler to close the how to window
  private void createHandlers() {
    closeLabel.setOnMousePressed(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        howToPane.setVisible(false);
      }
    });
  }


  public void setVisible() {
    howToPane.setVisible(true);
  }

}

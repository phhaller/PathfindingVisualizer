package util;

import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class StatusManager {

  private AnchorPane background;
  private static Label statusMsg;
  private static SequentialTransition current = null;

  public StatusManager(AnchorPane background) {
    this.background = background;
    createStatusMsg();
  }


  // Create the status message label.
  private void createStatusMsg() {

    statusMsg = new Label("Something went wrong..");
    statusMsg.setPrefSize(400, 50);
    statusMsg.setTranslateX(755);
    statusMsg.setTranslateY(740);
    statusMsg.setOpacity(0);
    statusMsg.setDisable(true);

    background.getChildren().add(statusMsg);

  }


  // Show the status label according to the type of the status message.
  public static void handleStatus(String type, String msg) {

    statusMsg.getStyleClass().clear();
    if (current != null) {
      current.stop();
      current = null;
    }

    if (type.equals("status")) {
      statusMsg.getStyleClass().add("statusMsg");
    }

    if (type.equals("error")) {
      statusMsg.getStyleClass().add("errorMsg");
    }

    statusMsg.setText(msg);

    FadeTransition ftIn = new FadeTransition(Duration.millis(500), statusMsg);
    ftIn.setFromValue(0);
    ftIn.setToValue(1);
    ftIn.setCycleCount(1);
    ftIn.setInterpolator(Interpolator.EASE_IN);

    FadeTransition ftOut = new FadeTransition(Duration.millis(500), statusMsg);
    ftOut.setFromValue(1);
    ftOut.setToValue(0);
    ftOut.setCycleCount(1);
    ftOut.setInterpolator(Interpolator.EASE_IN);

    SequentialTransition st = new SequentialTransition(ftIn, new PauseTransition(Duration.millis(2000)), ftOut);

    st.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        statusMsg.setOpacity(0);
      }
    });

    current = st;
    st.play();


  }


}

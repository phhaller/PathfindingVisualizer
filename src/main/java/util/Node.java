package util;

import javafx.scene.shape.Rectangle;

import java.util.HashMap;

public class Node {

  private Rectangle node;

  private int x;
  private int y;
  private int size;
  private String type = "none";
  private int distance = Integer.MAX_VALUE;
  private double gCost;
  private double hCost;
  private Node parent;
  private boolean eastJumpPoint = false;
  private boolean westJumpPoint = false;
  private boolean northJumpPoint = false;
  private boolean southJumpPoint = false;
  private HashMap<String, Integer> distancesMap;

  public Node(int x, int y, int size) {

    this.x = x;
    this.y = y;
    this.size = size;

    distancesMap = new HashMap<>();
    createNode();

  }


  // Create a new node.
  private void createNode() {

    node = new Rectangle(size, size);
    node.setTranslateX(x * size);
    node.setTranslateY(y * size + 70);

    node.getStyleClass().add("gridRec");

  }


  // Set the nodes type.
  public void setType(String type) {

    if (type.equals("none")) {
      node.getStyleClass().clear();
      node.getStyleClass().add("gridRec");
    }

    if (type.equals("start")) {
      node.getStyleClass().clear();
      node.getStyleClass().add("startNode");
    }

    if (type.equals("end")) {
      node.getStyleClass().clear();
      node.getStyleClass().add("endNode");
    }

    if (type.equals("obstacle")) {
      node.getStyleClass().clear();
      node.getStyleClass().add("obstacleNode");
    }

    this.type = type;
  }


  // Clear the nodes distance and parent.
  public void clear() {
    distance = Integer.MAX_VALUE;
    parent = null;
  }


  public Rectangle getNode() {
    return node;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public String getType() {
    return type;
  }

  public void setDistance(int distance) {
    this.distance = distance;
  }

  public int getDistance() {
    return distance;
  }

  public void setgCost(double gCost) {
    this.gCost = gCost;
  }

  public double getgCost() {
    return gCost;
  }

  public void sethCost(double hCost) {
    this.hCost = hCost;
  }

  public double gethCost() {
    return hCost;
  }

  public double getfCost() {
    return gCost + hCost;
  }

  public Node getParent() {
    return parent;
  }

  public void setParent(Node parent) {
    this.parent = parent;
  }

  public boolean isEastJumpPoint() {
    return eastJumpPoint;
  }

  public boolean isWestJumpPoint() {
    return westJumpPoint;
  }

  public boolean isNorthJumpPoint() {
    return northJumpPoint;
  }

  public boolean isSouthJumpPoint() {
    return southJumpPoint;
  }

  // Set a jump point by it's direction. Used in the pre-processing step in JPS+.
  public void setJumpPoint(String direction) {

    switch (direction) {
      case "EAST":
        eastJumpPoint = true;
        break;
      case "WEST":
        westJumpPoint = true;
        break;
      case "NORTH":
        northJumpPoint = true;
        break;
      case "SOUTH":
        southJumpPoint = true;
        break;
    }

  }

  public void addDistanceToMap(String direction, int distance) {
    distancesMap.put(direction, distance);
  }

  public int getDistanceFromMap(String direction) {
    return distancesMap.getOrDefault(direction, 0);
  }

  // Reset a node after a JPS+ search.
  public void resetNode() {
    gCost = 0;
    hCost = 0;
    parent = null;
    northJumpPoint = false;
    eastJumpPoint = false;
    southJumpPoint = false;
    westJumpPoint = false;
    distancesMap.clear();
  }
}

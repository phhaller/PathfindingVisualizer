package util;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class ResultManager {

  private AnchorPane background;
  private AnchorPane resultPane;
  private Label closeLabel;
  private String algorithm;
  private String heuristics;
  private String diagonal;
  private String speed;
  private String duration;
  private String open;
  private String closed;

  private double orgSceneX, orgSceneY;
  private double orgTranslateX, orgTranslateY;

  public ResultManager(AnchorPane background, String algorithm, String heuristics, String diagonal, String speed, String duration, String open, String closed) {
    this.background = background;
    this.algorithm = algorithm;
    this.heuristics = heuristics.substring(0, 1).toUpperCase() + heuristics.substring(1).toLowerCase();
    this.diagonal = diagonal.substring(0, 1).toUpperCase() + diagonal.substring(1).toLowerCase();
    this.speed = speed;
    this.duration = duration;
    this.open = open;
    this.closed = closed;

    createResultPane();
    createHandler();
  }


  // Create a new result window.
  private void createResultPane() {

    resultPane = new AnchorPane();
    resultPane.setPrefSize(230, 260);
    resultPane.setTranslateX(20);
    resultPane.setTranslateY(520);
    resultPane.getStyleClass().add("resultPaneBackground");

    closeLabel = new Label();
    closeLabel.setTranslateX(184);
    closeLabel.setTranslateY(20);
    closeLabel.setGraphic(new ImageView(new Image("/pictures/closeIconSmall.png")));
    closeLabel.getStyleClass().add("closeLabel");

    Label resTitleLabel = new Label("Results");
    resTitleLabel.setPrefSize(120, 50);
    resTitleLabel.setTranslateX(50);
    resTitleLabel.setTranslateY(10);
    resTitleLabel.getStyleClass().add("resultsTitleLabel");

    Label algo = new Label("Algorithm: ");
    algo.setTranslateX(20);
    algo.setTranslateY(70);
    algo.getStyleClass().add("resultsLabel");

    Label algoVal = new Label(algorithm);
    algoVal.setTranslateX(130);
    algoVal.setTranslateY(70);
    algoVal.getStyleClass().add("resultsLabel");


    Label heur = new Label("Heuristics: ");
    heur.setTranslateX(20);
    heur.setTranslateY(95);
    heur.getStyleClass().add("resultsLabel");

    Label heurVal = new Label(heuristics);
    heurVal.setTranslateX(130);
    heurVal.setTranslateY(95);
    heurVal.getStyleClass().add("resultsLabel");


    Label diag = new Label("Diagonal: ");
    diag.setTranslateX(20);
    diag.setTranslateY(120);
    diag.getStyleClass().add("resultsLabel");

    Label diagVal = new Label(diagonal);
    diagVal.setTranslateX(130);
    diagVal.setTranslateY(120);
    diagVal.getStyleClass().add("resultsLabel");


    Label sped = new Label("Speed: ");
    sped.setTranslateX(20);
    sped.setTranslateY(145);
    sped.getStyleClass().add("resultsLabel");

    Label spedVal = new Label(speed);
    spedVal.setTranslateX(130);
    spedVal.setTranslateY(145);
    spedVal.getStyleClass().add("resultsLabel");


    Label dur = new Label("Duration (sec): ");
    dur.setTranslateX(20);
    dur.setTranslateY(170);
    dur.getStyleClass().add("resultsLabel");

    Label durVal = new Label(duration);
    durVal.setTranslateX(130);
    durVal.setTranslateY(170);
    durVal.getStyleClass().add("resultsLabel");


    Label op = new Label("OpenList size: ");
    op.setTranslateX(20);
    op.setTranslateY(195);
    op.getStyleClass().add("resultsLabel");

    Label opVal = new Label(open);
    opVal.setTranslateX(130);
    opVal.setTranslateY(195);
    opVal.getStyleClass().add("resultsLabel");


    Label clos = new Label("ClosedList size: ");
    clos.setTranslateX(20);
    clos.setTranslateY(220);
    clos.getStyleClass().add("resultsLabel");

    Label closVal = new Label(closed);
    closVal.setTranslateX(130);
    closVal.setTranslateY(220);
    closVal.getStyleClass().add("resultsLabel");



    resultPane.getChildren().addAll(closeLabel, resTitleLabel, algo, algoVal, heur, heurVal, diag, diagVal, sped,
        spedVal, dur, durVal, op, opVal,
        clos, closVal);

    background.getChildren().add(resultPane);

  }


  // Create the handler to close the result window.
  private void createHandler() {

    closeLabel.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        background.getChildren().remove(resultPane);
      }
    });


    resultPane.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        orgSceneX = event.getSceneX();
        orgSceneY = event.getSceneY();
        orgTranslateX = ((javafx.scene.Node)(event.getSource())).getTranslateX();
        orgTranslateY = ((Node)(event.getSource())).getTranslateY();
      }
    });


    resultPane.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        double offsetX = event.getSceneX() - orgSceneX;
        double offsetY = event.getSceneY() - orgSceneY;
        double newTranslateX = orgTranslateX + offsetX;
        double newTranslateY = orgTranslateY + offsetY;

        ((Node)(event.getSource())).setTranslateX(newTranslateX);
        ((Node)(event.getSource())).setTranslateY(newTranslateY);
      }
    });


    resultPane.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

      }
    });

  }


}

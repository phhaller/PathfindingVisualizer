package view;

import javafx.animation.Animation;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import search.*;
import util.*;

import java.util.ArrayList;

public class MainWindowController {

  @FXML
  private AnchorPane background;

  @FXML
  private Label howToLabel, startLabel, pauseLabel, speedLabel, clearLabel;

  @FXML
  private CheckBox animationCheckBox;

  @FXML
  private Slider speedSlider;

  @FXML
  private ComboBox algoChooser, mapChooser, heuristicsChooser, diagonalChooser;

  private HowToManager howToManager;
  private Node[][] nodes;
  private Node start = null;
  private Node end = null;
  private ArrayList<Node> obstacles = new ArrayList<>();

  private boolean startDragging = false;
  private boolean endDragging = false;

  private boolean animationCleared = true;

  private Timeline algoTimeline = null;


  // Initialize the PathFinder window and all necessary components.
  public void initialize() {

    createSlider();
    createChooser();
    createGrid();
    createStartAndEnd();
    createHandlers();

    howToManager = new HowToManager(background);
    StatusManager statusManager = new StatusManager(background);

  }


  // Create the slider to set the animation speed.
  private void createSlider() {
    animationCheckBox.setSelected(true);
    speedSlider.setMin(1);
    speedSlider.setMax(10);
    speedSlider.setValue(1);
    speedSlider.setBlockIncrement(1);
    speedSlider.setMajorTickUnit(1);
    speedSlider.setMinorTickCount(0);
    speedSlider.setSnapToTicks(true);

    speedSlider.valueProperty().addListener((obs, oldval, newVal) ->
        setSlider(newVal));
  }

  // Set the animation speed label according to the slider value.
  private void setSlider(Number value) {
    int val = value.intValue();
    speedSlider.setValue(val);
    speedLabel.setText(String.valueOf(val));
  }


  // Create all the combo boxes to choose map, algorithm, heuristics, and diagonal strategy.
  private void createChooser() {
    mapChooser.getItems().addAll("Map", "Empty", "Maze", "Map1", "Map2", "Map3", "Map4", "Map5", "Map6",
        "Map7", "Map8", "Map9", "Map10", "Map11", "Map12");
    mapChooser.getSelectionModel().select("Map");
    mapChooser.getStyleClass().add("mapChooser");

    algoChooser.getItems().addAll( "Algorithm", "Breadth First Search", "Dijkstra", "Greedy BFS", "A*",
        "JPS", "JPS+");
    algoChooser.getSelectionModel().select("Algorithm");

    heuristicsChooser.getItems().addAll("Heuristics", "Manhattan", "Euclidean", "Octile", "Chebyshev");
    heuristicsChooser.getSelectionModel().select("Heuristics");
    heuristicsChooser.setDisable(true);

    diagonalChooser.getItems().addAll("Diagonal", "Always", "Never", "One_obstacle", "No_obstacles");
    diagonalChooser.getSelectionModel().select("Diagonal");
  }


  // Create the grid and all its nodes.
  private void createGrid() {

    int size = 10;
    int width = 1200;
    int height = 730;

    nodes = new Node[width / size][height / size];

    for (int y = 0; y < height/size; y++) {
      for (int x = 0; x < width/size; x++) {

        Node node = new Node(x, y, size);

        createRecHandler(node);

        nodes[x][y] = node;
        background.getChildren().add(node.getNode());

      }
    }

  }


  // Set the start and the end node.
  private void createStartAndEnd() {

    start = nodes[16][12];
    start.setType("start");

    end = nodes[102][55];
    end.setType("end");

  }


  // Create the handlers that control moving the start and end nodes, as well as the creation and deletion of obstacles.
  private void createRecHandler(Node node) {

    node.getNode().addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (algoTimeline == null || animationCleared) {

          if (node == start) {
            startDragging = true;
          } else if (node == end) {
            endDragging = true;
          } else {
            if (node.getType().equals("obstacle")) {
              node.setType("none");
              obstacles.remove(node);
            } else {
              node.setType("obstacle");
              obstacles.add(node);
            }
          }

        }

      }
    });


    node.getNode().setOnDragDetected(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (algoTimeline == null || animationCleared) {
          Rectangle r = (Rectangle) event.getSource();
          r.startFullDrag();
        }

      }
    });

    node.getNode().setOnMouseDragEntered(new EventHandler<MouseDragEvent>() {
      @Override
      public void handle(MouseDragEvent event) {

        if (algoTimeline == null || animationCleared) {

          if (startDragging && !node.getType().equals("obstacle") && !node.getType().equals("end")) {
            start.setType("none");
            start = node;
            start.setType("start");
          } else if (endDragging && !node.getType().equals("obstacle") && !node.getType().equals("start")) {
            end.setType("none");
            end = node;
            end.setType("end");
          } else {
            if (node.getType().equals("obstacle") && !startDragging && !endDragging &&
                !node.getType().equals("start") && !node.getType().equals("end")) {
              node.setType("none");
              obstacles.remove(node);
            } else if (!startDragging && !endDragging &&
                !node.getType().equals("start") && !node.getType().equals("end")) {
              node.setType("obstacle");
              obstacles.add(node);
            }
          }
        }

      }
    });

    node.getNode().setOnMouseDragReleased(new EventHandler<MouseDragEvent>() {
      @Override
      public void handle(MouseDragEvent event) {

        if (algoTimeline == null || animationCleared) {
          if (startDragging) {
            if (!node.getType().equals("obstacle")) {
              start.setType("none");
              start = node;
              System.out.println(start.getX() + " : " + start.getY());
              start.setType("start");
            }
            startDragging = false;
          }

          if (endDragging) {
            if (!node.getType().equals("obstacle")) {
              end.setType("none");
              end = node;
              System.out.println(end.getX() + " : " + end.getY());
              end.setType("end");
            }
            endDragging = false;
          }
        }

      }
    });

  }


  // Create the handlers that control the labels and combo boxes in the menu.
  private void createHandlers() {

    howToLabel.setOnMousePressed(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        howToManager.setVisible();
      }
    });


    mapChooser.getSelectionModel().selectedItemProperty().addListener((obs, oldval, newVal) ->
        setObstacles((String) newVal));

    algoChooser.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal) ->
        setActiveChoosers((String) newVal));

    animationCheckBox.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (animationCheckBox.isSelected()) {
          speedSlider.setDisable(false);
          speedLabel.setDisable(false);
        } else {
          speedSlider.setDisable(true);
          speedLabel.setDisable(true);
        }
      }
    });

    startLabel.setOnMousePressed(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (algoChooser.getValue().equals("Algorithm")) {
          StatusManager.handleStatus("error", "Choose an algorithm!");
        } else if (algoTimeline != null && algoTimeline.getStatus().equals(Animation.Status.PAUSED)) {

          algoTimeline.play();
          StatusManager.handleStatus("status", "The search continues!");

        } else if (algoTimeline == null || algoTimeline.getStatus().equals(Animation.Status.STOPPED)) {

          clearAnimation();

          StatusManager.handleStatus("status", "The search has started!");

          String heuristics = heuristicsChooser.getValue().equals("Heuristics") ? "Manhattan" : (String) heuristicsChooser.getValue();
          String diagonal = diagonalChooser.getValue().equals("Diagonal") ? "Always" : (String) diagonalChooser.getValue();

          if (algoChooser.getValue().equals("Breadth First Search")) {
            BreadthFirstSearch bfs = new BreadthFirstSearch(nodes, start, end,
                Integer.valueOf(speedLabel.getText()), diagonal, background);
            if (animationCheckBox.isSelected()) {
              bfs.start();
              algoTimeline = bfs.getTimeline();
            } else {
              bfs.startWithoutAnimation();
            }
          } else if (algoChooser.getValue().equals("Dijkstra")) {
            Dijkstra dijkstra = new Dijkstra(nodes, start, end, Integer.valueOf(speedLabel.getText()), diagonal, background);
            if (animationCheckBox.isSelected()) {
              dijkstra.start();
              algoTimeline = dijkstra.getTimeline();
            } else {
              dijkstra.startWithoutAnimation();
            }
          } else if (algoChooser.getValue().equals("Greedy BFS")) {
            BestFirstSearch bestFirstSearch = new BestFirstSearch(nodes, start, end, Integer.valueOf(speedLabel.getText()), diagonal, heuristics, background);
            if (animationCheckBox.isSelected()) {
              bestFirstSearch.start();
              algoTimeline = bestFirstSearch.getTimeline();
            } else {
              bestFirstSearch.startWithoutAnimation();
            }
          } else if (algoChooser.getValue().equals("A*")) {
            Astar astar = new Astar(nodes, start, end, Integer.valueOf(speedLabel.getText()), diagonal, heuristics, background);
            if (animationCheckBox.isSelected()) {
              astar.start();
              algoTimeline = astar.getTimeline();
            } else {
              astar.startWithoutAnimation();
            }
          } else if (algoChooser.getValue().equals("JPS")) {
            JPS jps = null;
            switch (diagonal) {
              case "Always":
                jps = new JPSDiagAlways(nodes, start, end, Integer.valueOf(speedLabel.getText()), heuristics, diagonal, background);
                break;
              case "Never":
                jps = new JPSDiagNever(nodes, start, end, Integer.valueOf(speedLabel.getText()), heuristics, diagonal, background);
                break;
              case "One_obstacle":
                jps = new JPSDiagOneObstacle(nodes, start, end, Integer.valueOf(speedLabel.getText()), heuristics, diagonal, background);
                break;
              case "No_obstacles":
                jps = new JPSDiagNoObstacles(nodes, start, end, Integer.valueOf(speedLabel.getText()), heuristics, diagonal, background);
                break;
            }
            if (animationCheckBox.isSelected()) {
              jps.start();
              algoTimeline = jps.getTimeline();
            } else {
              jps.startWithoutAnimation();
            }
          } else if (algoChooser.getValue().equals("JPS+")) {
            JPSplus jpsplus = new JPSplus(nodes, start, end, Integer.valueOf(speedLabel.getText()), background);
            if (animationCheckBox.isSelected()) {
              jpsplus.start();
              algoTimeline = jpsplus.getTimeline();
            } else {
              jpsplus.startWithoutAnimation();
            }
          }

          animationCleared = false;

        }

      }
    });

    pauseLabel.setOnMousePressed(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        if (algoTimeline != null) {
          algoTimeline.pause();
          StatusManager.handleStatus("status", "The search has been paused!");
        }
      }
    });


    clearLabel.setOnMousePressed(new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (animationCleared) {
          mapChooser.getSelectionModel().select("Empty");
          clearGrid();
        } else {

          if (algoTimeline != null && algoTimeline.getStatus().equals(Animation.Status.RUNNING)) {
            algoTimeline.stop();
            clearAnimation();
            animationCleared = true;
          } else {
            clearAnimation();
            animationCleared = true;
          }
        }

      }
    });

  }


  // Set the obstacles according to the map that was chosen.
  private void setObstacles(String map) {

    clearGrid();

    start.setType("none");
    end.setType("none");

    switch (map) {

      case "Map1":
        start = nodes[41][17];
        start.setType("start");

        end = nodes[80][55];
        end.setType("end");
        break;
      case "Map2":
        start = nodes[63][14];
        start.setType("start");

        end = nodes[72][40];
        end.setType("end");
        break;
      case "Map3":
        start = nodes[39][2];
        start.setType("start");

        end = nodes[44][67];
        end.setType("end");
        break;
      case "Map4":
        start = nodes[34][2];
        start.setType("start");

        end = nodes[84][65];
        end.setType("end");
        break;
      case "Map5":
        start = nodes[43][4];
        start.setType("start");

        end = nodes[87][36];
        end.setType("end");
        break;
      case "Map6":
        start = nodes[31][62];
        start.setType("start");

        end = nodes[84][7];
        end.setType("end");
        break;
      case "Map7":
        start = nodes[23][36];
        start.setType("start");

        end = nodes[91][36];
        end.setType("end");
        break;
      case "Map8":
        start = nodes[33][24];
        start.setType("start");

        end = nodes[86][44];
        end.setType("end");
        break;
      case "Map9":
        start = nodes[32][52];
        start.setType("start");

        end = nodes[84][21];
        end.setType("end");
        break;
      case "Map10":
        start = nodes[33][18];
        start.setType("start");

        end = nodes[104][42];
        end.setType("end");
        break;
      case "Map11":
        start = nodes[18][4];
        start.setType("start");

        end = nodes[94][24];
        end.setType("end");
        break;
      case "Map12":
        start = nodes[56][1];
        start.setType("start");

        end = nodes[39][54];
        end.setType("end");
        break;

    }

    if (!map.equals("Empty") && !map.equals("Map")) {

      if (map.equals("Maze")) {

        MazeManager mazeManager = new MazeManager(60, 36);
        String[][] maze = mazeManager.getMap();

        for (int i = 0; i < maze.length; i++) {
          for (int j = 0; j < maze[0].length; j++) {

            if (maze[i][j].equals("obstacle")) {
              nodes[i][j].setType("obstacle");
              obstacles.add(nodes[i][j]);
            }

          }
        }

        createStartAndEnd();
      } else {

        MapReader mapReader = new MapReader(map.toLowerCase() + ".map");
        String[][] csvMap = mapReader.getCsvMap();

        int x = 120 / 2 - csvMap.length / 2;
        int y = 73 / 2 - csvMap[0].length / 2;

        for (int xx = 0; xx < csvMap.length; xx++) {
          for (int yy = 0; yy < csvMap[0].length; yy++) {

            if (csvMap[xx][yy].equals("obstacle")) {

              nodes[x + xx][y + yy].setType("obstacle");
              obstacles.add(nodes[x + xx][y + yy]);

            }

          }
        }

      }

    } else {

      start.setType("none");
      end.setType("none");
      for (Node n : obstacles) {
        n.setType("none");
      }
      obstacles.clear();

      createStartAndEnd();

    }

    StatusManager.handleStatus("status", map + " loaded!");

  }


  // Enable or disable combo boxes according to the algorithm.
  private void setActiveChoosers(String algorithm) {

    switch (algorithm) {

      case "Breadth First Search":
      case "Dijkstra":
        heuristicsChooser.setDisable(true);
        diagonalChooser.setDisable(false);
        break;
      case "Greedy BFS":
      case "A*":
      case "JPS":
        heuristicsChooser.setDisable(false);
        diagonalChooser.setDisable(false);
        break;
      case "JPS+":
        heuristicsChooser.setDisable(true);
        diagonalChooser.setDisable(true);
        break;

    }

  }


  // Clear the styling of the nodes that were set during a search.
  private void clearAnimation() {
    algoTimeline = null;

    for (int y = 0; y < nodes[0].length; y++) {
      for (int x = 0; x < nodes.length; x++) {

        Node node = nodes[x][y];

        if (node != start && node != end && !obstacles.contains(node)) {
          node.getNode().getStyleClass().clear();
          node.getNode().getStyleClass().add("gridRec");
        }
        node.clear();

      }
    }

    StatusManager.handleStatus("status", "The previous search has been cleared!");

  }


  // Clear all obstacles on the grid and create an empty amp.
  private void clearGrid() {

    algoTimeline = null;

    for (int y = 0; y < nodes[0].length; y++) {
      for (int x = 0; x < nodes.length; x++) {

        Node node = nodes[x][y];

        if (node != start && node != end) {
          node.getNode().getStyleClass().clear();
          node.getNode().getStyleClass().add("gridRec");
        }
        node.clear();

      }
    }

    for (Node n : obstacles) {
      n.setType("none");
    }
    obstacles.clear();

    StatusManager.handleStatus("status", "The whole grid has been cleared!");

  }

}

package search;

import javafx.scene.layout.AnchorPane;
import util.Node;
import util.Util;

import java.util.ArrayList;

public class JPSDiagNever extends JPS {

  public JPSDiagNever(Node[][] nodes, Node start, Node end, int speed, String heuristics, String diagonal,
                      AnchorPane background) {
    super(nodes, start, end, speed, heuristics, diagonal, background);
  }


  @Override
  protected ArrayList<Node> getNeighboursPruned(Node node) {

    ArrayList<Node> neighbours = new ArrayList<>();

    Node parent = node.getParent();
    int x = node.getX();
    int y = node.getY();
    int px, py, dx, dy;

    if (parent != null) {

      px = parent.getX();
      py = parent.getY();

      dx = (x - px) / Math.max(Math.abs(x - px), 1);
      dy = (y - py) / Math.max(Math.abs(y - py), 1);

      if (dx != 0) {
        if (isWalkable(x + dx, y)) {
          neighbours.add(nodes[x + dx][y]);
        }

        if (isWalkable(x, y + 1)) {
          neighbours.add(nodes[x][y + 1]);
        }

        if (isWalkable(x, y - 1)) {
          neighbours.add(nodes[x][y - 1]);
        }

      } else if (dy != 0) {
        if (isWalkable(x, y + dy)) {
          neighbours.add(nodes[x][y + dy]);
        }

        if (isWalkable(x + 1, y)) {
          neighbours.add(nodes[x + 1][y]);
        }

        if (isWalkable(x - 1, y)) {
          neighbours.add(nodes[x - 1][y]);
        }

      }

    } else {
      return Util.getNeighbours(nodes, node, "NEVER");
    }

    return neighbours;

  }


  @Override
  protected Node jump(int x, int y, int px, int py, boolean animated) {

    int dx = (x - px) / Math.max(Math.abs(x - px), 1);
    int dy = (y - py) / Math.max(Math.abs(y - py), 1);

    if (isWalkable(x, y) && nodes[x][y] != end && animated) {
      nodes[x][y].getNode().getStyleClass().add("jpsNeighbour");
    }

    if (!isWalkable(x, y)) {
      return null;
    }

    if (x == end.getX() && y == end.getY()) {
      return nodes[x][y];
    }

    if (dx != 0) {
      if ((isWalkable(x, y + 1) && !isWalkable(x - dx, y + 1)) ||
          (isWalkable(x, y - 1) && !isWalkable(x - dx, y - 1))) {
        return nodes[x][y];
      }
    } else if (dy != 0) {
      if ((isWalkable(x + 1, y) && !isWalkable(x + 1, y - dy)) ||
          (isWalkable(x - 1, y) && !isWalkable(x - 1, y - dy))) {
        return nodes[x][y];
      }

      if (jump(x + 1, y, x, y, animated) != null ||
          jump(x - 1, y, x, y, animated) != null) {
        return nodes[x][y];
      }
    } else {
      return null;
    }

    return jump(x + dx, y + dy, x, y, animated);

  }

}

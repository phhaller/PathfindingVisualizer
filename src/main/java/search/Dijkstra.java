package search;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import util.Node;
import util.StatusManager;
import util.Util;

import java.util.ArrayList;

public class Dijkstra {

  private Node[][] nodes;
  private Node start;
  private Node end;
  private ArrayList<Node> closedList = new ArrayList<>();
  private ArrayList<Node> openList = new ArrayList<>();
  private int speed;
  private Timeline timeline;
  private float duration = 0f;
  private AnchorPane background;
  private String diagonal;

  public Dijkstra(Node[][] nodes, Node start, Node end, int speed, String diagonal, AnchorPane background) {

    this.nodes = nodes;
    this.start = start;
    this.end = end;
    this.speed = speed;
    this.diagonal = diagonal.toUpperCase();
    this.background = background;

  }


  // Start an animated search.
  public void start() {

    long startTime = System.nanoTime();

    openList.add(start);
    start.setDistance(0);
    start.setParent(null);

    timeline = new Timeline(new KeyFrame(Duration.millis(10 - (speed - 1)), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        if (!openList.isEmpty()) {

          Node evalNode = getNodeWithLowestDistance();

          if (evalNode == end) {
            duration = (System.nanoTime() - startTime) / 1000000000f;
            StatusManager.handleStatus("status", "Path has been found! Displaying results..");
            Util.constructPath(end, start, end, background, "Dijkstra", "-", diagonal, speed,
                duration, openList, closedList);
            timeline.stop();
          }

          openList.remove(evalNode);
          closedList.add(evalNode);
          if (evalNode != start && evalNode != end) {
            evalNode.getNode().getStyleClass().add("closed");
          }

          evaluateNeighbours(evalNode, true);

        } else {
          StatusManager.handleStatus("error", "The end node was not found!");
          timeline.stop();
        }


      }
    }));

    timeline.setCycleCount(Animation.INDEFINITE);
    timeline.play();

  }


  // Start a search without animation.
  public void startWithoutAnimation() {

    long startTime = System.nanoTime();
    boolean found = false;

    openList.add(start);
    start.setDistance(0);
    start.setParent(null);

    while (!openList.isEmpty()) {

      Node evalNode = getNodeWithLowestDistance();

      if (evalNode == end) {
        found = true;
        duration = (System.nanoTime() - startTime) / 1000000000f;
        StatusManager.handleStatus("status", "Path has been found! Displaying results..");
        Util.constructPath(end, start, end, background, "Dijkstra", "-", diagonal, 0,
            duration, openList, closedList);
      }

      openList.remove(evalNode);
      closedList.add(evalNode);

      evaluateNeighbours(evalNode, false);

    }

    if (!found) {
      StatusManager.handleStatus("error", "The end node was not found!");
    }

  }


  // Get the node from the open list with the lowest distance value.
  private Node getNodeWithLowestDistance() {

    int lowest = Integer.MAX_VALUE;
    Node lowestNode = null;

    for (Node n : openList) {
      if (n.getDistance() < lowest) {
        lowest = n.getDistance();
        lowestNode = n;
      }
    }

    return lowestNode;

  }


  // Get the neighbours of a node, calculate their distances and add them to the open list if necessary.
  private void evaluateNeighbours(Node node, boolean animted) {

    ArrayList<Node> neighbours = Util.getNeighbours(nodes, node, diagonal);
    for (Node n : neighbours) {

      if (!closedList.contains(n) && !openList.contains(n) && !n.getType().equals("obstacle")) {

        int dist = getDistance(node, n);
        int newDist = node.getDistance() + dist;
        if (n.getDistance() > newDist) {
          n.setDistance(newDist);
          openList.add(n);
          n.setParent(node);
          if (animted) {
            if (n != start && n != end) {
              n.getNode().getStyleClass().add("open");
            }
          }
          // Util.animateNodeColoring(n, start, end, speed);
        }

      }

    }

  }


  // Get the distance between two nodes.
  private int getDistance(Node s, Node e) {

    int dist = 0;

    if (s.getX() == e.getX() || s.getY() == e.getY()) {
      dist = 10;
    } else {
      dist = 14;
    }

    return dist;

  }


  public Timeline getTimeline() {
    return timeline;
  }


}

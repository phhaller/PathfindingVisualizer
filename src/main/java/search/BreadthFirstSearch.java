package search;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import util.Node;
import util.StatusManager;
import util.Util;

import java.util.ArrayList;
import java.util.LinkedList;

public class BreadthFirstSearch {

  private Node[][] nodes;
  private Node start;
  private Node end;
  private LinkedList<Node> closedList = new LinkedList<>();
  private LinkedList<Node> openList = new LinkedList<>();
  private int speed;
  private Timeline timeline;
  private float duration = 0f;
  private AnchorPane background;
  private String diagonal;

  public BreadthFirstSearch(Node[][] nodes, Node start, Node end, int speed, String diagonal, AnchorPane background) {

    this.nodes = nodes;
    this.start = start;
    this.end = end;
    this.speed = speed;
    this.diagonal = diagonal.toUpperCase();
    this.background = background;

  }


  // Start an animated search.
  public void start() {

    long startTime = System.nanoTime();
    openList.add(start);
    start.setParent(null);

    timeline = new Timeline(new KeyFrame(Duration.millis(10 - (speed - 1)), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        if (!openList.isEmpty()) {

          Node node = openList.removeFirst();
          if (node == end) {
            duration = (System.nanoTime() - startTime) / 1000000000f;
            StatusManager.handleStatus("status", "Path has been found! Displaying results..");
            Util.constructPath(end, start, end, background, "BreadthFS", "-", diagonal, speed, duration, openList, closedList);
            timeline.stop();
          } else {
            closedList.add(node);
            if (node != start && node != end) {
              node.getNode().getStyleClass().add("closed");
            }
            addNeighboursToOpenList(node, true);
          }

        } else {
          StatusManager.handleStatus("error", "The end node was not found!");
          timeline.stop();
        }

      }
    }));

    timeline.setCycleCount(Animation.INDEFINITE);
    timeline.play();

  }


  // Start a search without animation.
  public void startWithoutAnimation() {

    long startTime = System.nanoTime();
    boolean found = false;
    openList.add(start);
    start.setParent(null);

    while (!openList.isEmpty()) {
      Node node = openList.removeFirst();
      if (node == end) {
        found = true;
        duration = (System.nanoTime() - startTime) / 1000000000f;
        StatusManager.handleStatus("status", "Path has been found! Displaying results..");
        Util.constructPath(end, start, end, background, "BreadthFS", "-", diagonal, 0, duration, openList, closedList);
        break;
      } else {
        closedList.add(node);
        addNeighboursToOpenList(node, false);
      }
    }

    if (!found) {
      StatusManager.handleStatus("error", "The end node was not found!");
    }

  }


  // Get the neighbours of a node and add them to the open list.
  private void addNeighboursToOpenList(Node node, boolean animated) {

    ArrayList<Node> neighbours = Util.getNeighbours(nodes, node, diagonal);

    for (Node n : neighbours) {
      checkAddToOpenList(n, node, animated);
    }

  }


  // Check if a neighbour should be added to the open list and style it if necessary.
  private void checkAddToOpenList(Node node, Node parent, boolean animated) {
    if (!closedList.contains(node) && !openList.contains(node) && !node.getType().equals("obstacle")) {
      node.setParent(parent);
      openList.add(node);
      if (animated) {
        if (node != start && node != end) {
          node.getNode().getStyleClass().add("open");
        }
      }
      // Util.animateNodeColoring(node, start, end, speed);
    }
  }


  public Timeline getTimeline() {
    return timeline;
  }


}

package search;

import javafx.scene.layout.AnchorPane;
import util.Node;
import util.Util;

import java.util.ArrayList;

public class JPSDiagAlways extends JPS {

  public JPSDiagAlways(Node[][] nodes, Node start, Node end, int speed, String heuristics, String diagonal,
                       AnchorPane background) {
    super(nodes, start, end, speed, heuristics, diagonal, background);
  }


  @Override
  protected ArrayList<Node> getNeighboursPruned(Node node) {

    ArrayList<Node> neighbours = new ArrayList<>();

    Node parent = node.getParent();
    int x = node.getX();
    int y = node.getY();
    int px, py, dx, dy;

    if (parent != null) {

      px = parent.getX();
      py = parent.getY();

      dx = (x - px) / Math.max(Math.abs(x - px), 1);
      dy = (y - py) / Math.max(Math.abs(y - py), 1);

      if (dx != 0 && dy != 0) {
        if (isWalkable(x, y + dy)) {
          neighbours.add(nodes[x][y + dy]);
        }

        if (isWalkable(x + dx, y)) {
          neighbours.add(nodes[x + dx][y]);
        }

        if (isWalkable(x + dx, y + dy)) {
          neighbours.add(nodes[x + dx][y + dy]);
        }

        if (!isWalkable(x - dx, y)) {
          neighbours.add(nodes[x - dx][y + dy]);
        }

        if (!isWalkable(x, y - dy)) {
          neighbours.add(nodes[x + dx][y - dy]);
        }

      } else {
        if (dx == 0) {
          if (isWalkable(x, y + dy)) {
            neighbours.add(nodes[x][y + dy]);
          }

          if (!isWalkable(x + 1, y)) {
            neighbours.add(nodes[x + 1][y + dy]);
          }

          if (!isWalkable(x - 1, y)) {
            neighbours.add(nodes[x - 1][y + dy]);
          }

        } else {
          if (isWalkable(x + dx, y)) {
            neighbours.add(nodes[x + dx][y]);
          }

          if (!isWalkable(x, y + 1)) {
            neighbours.add(nodes[x + dx][y + 1]);
          }

          if (!isWalkable(x, y - 1)) {
            neighbours.add(nodes[x + dx][y - 1]);
          }

        }
      }

    } else {
      return Util.getNeighbours(nodes, node, "ALWAYS");
    }

    return neighbours;
  }


  @Override
  protected Node jump(int x, int y, int px, int py, boolean animated) {

    int dx = (x - px) / Math.max(Math.abs(x - px), 1);
    int dy = (y - py) / Math.max(Math.abs(y - py), 1);

    if (isWalkable(x, y) && nodes[x][y] != end && animated) {
      nodes[x][y].getNode().getStyleClass().add("jpsNeighbour");
    }

    if (!isWalkable(x, y)) {
      return null;
    }

    if (x == end.getX() && y == end.getY()) {
      return nodes[x][y];
    }

    if (dx != 0 && dy != 0) {
      if ((isWalkable(x - dx, y + dy) && !isWalkable(x - dx, y)) ||
          (isWalkable(x + dx, y - dy) && !isWalkable(x, y - dy))) {
        return nodes[x][y];
      }

      if (jump(x + dx, y, x, y, animated) != null ||
          jump(x, y + dy, x, y, animated) != null) {
        return nodes[x][y];
      }
    } else {
      if (dx != 0) {
        if ((isWalkable(x + dx, y + 1) && !isWalkable(x, y + 1)) ||
            (isWalkable(x + dx, y - 1) && !isWalkable(x, y - 1))) {
          return nodes[x][y];
        }
      } else {
        if ((isWalkable(x + 1, y + dy) && !isWalkable(x + 1, y)) ||
            (isWalkable(x - 1, y + dy) && !isWalkable(x - 1, y))) {
          return nodes[x][y];
        }
      }
    }

    return jump(x + dx, y + dy, x, y, animated);
  }

}

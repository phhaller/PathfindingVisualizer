package search;

import javafx.scene.layout.AnchorPane;
import util.Node;
import util.Util;

import java.util.ArrayList;

public class JPSDiagNoObstacles extends JPS {

  public JPSDiagNoObstacles(Node[][] nodes, Node start, Node end, int speed, String heuristics, String diagonal, AnchorPane background) {
    super(nodes, start, end, speed, heuristics, diagonal, background);
  }


  @Override
  protected ArrayList<Node> getNeighboursPruned(Node node) {

    ArrayList<Node> neighbours = new ArrayList<>();

    Node parent = node.getParent();
    int x = node.getX();
    int y = node.getY();
    int px, py, dx, dy;

    if (parent != null) {

      px = parent.getX();
      py = parent.getY();

      dx = (x - px) / Math.max(Math.abs(x - px), 1);
      dy = (y - py) / Math.max(Math.abs(y - py), 1);

      if ((dx & dy) != 0) {
        if (isWalkable(x, y + dy)) {
          neighbours.add(nodes[x][y + dy]);
        }

        if (isWalkable(x + dx, y)) {
          neighbours.add(nodes[x + dx][y]);
        }

        if (isWalkable(x, y + dy) && isWalkable(x + dx, y)) {
          neighbours.add(nodes[x + dx][y + dy]);
        }

      } else {
        if (dx != 0) {
          boolean nextWalkable = isWalkable(x + dx, y);
          boolean topWalkable = isWalkable(x, y + 1);
          boolean bottomWalkable = isWalkable(x, y - 1);

          if (nextWalkable) {
            neighbours.add(nodes[x + dx][y]);
            if (topWalkable) {
              neighbours.add(nodes[x + dx][y + 1]);
            }

            if (bottomWalkable) {
              neighbours.add(nodes[x + dx][y - 1]);
            }
          }
          if (topWalkable) {
            neighbours.add(nodes[x][y + 1]);
          }

          if (bottomWalkable) {
            neighbours.add(nodes[x][y - 1]);
          }
        } else if (dy != 0) {
          boolean nextWalkable = isWalkable(x, y + dy);
          boolean rightWalkable = isWalkable(x + 1 , y);
          boolean leftWalkable = isWalkable(x - 1, y);

          if (nextWalkable) {
            neighbours.add(nodes[x][y + dy]);
            if (rightWalkable) {
              neighbours.add(nodes[x + 1][y + dy]);
            }

            if (leftWalkable) {
              neighbours.add(nodes[x - 1][y + dy]);
            }
          }
          if (rightWalkable) {
            neighbours.add(nodes[x + 1][y]);
          }

          if (leftWalkable) {
            neighbours.add(nodes[x - 1][y]);
          }
        }
      }

    } else {
      return Util.getNeighbours(nodes, node, "NO_OBSTACLES");
    }

    return neighbours;
  }


  @Override
  protected Node jump(int x, int y, int px, int py, boolean animated) {

    int dx = (x - px) / Math.max(Math.abs(x - px), 1);
    int dy = (y - py) / Math.max(Math.abs(y - py), 1);

    if (isWalkable(x, y) && nodes[x][y] != end && animated) {
      nodes[x][y].getNode().getStyleClass().add("jpsNeighbour");
    }

    if (!isWalkable(x, y)) {
      return null;
    }

    if (x == end.getX() && y == end.getY()) {
      return nodes[x][y];
    }

    if ((dx & dy) != 0) {
      if (jump(x + dx, y, x, y, animated) != null ||
          jump(x, y + dy, x, y, animated) != null) {
        return nodes[x][y];
      }
    } else {
      if (dx != 0) {
        if ((isWalkable(x, y - 1) && !isWalkable(x - dx, y - 1)) ||
            (isWalkable(x, y + 1) && !isWalkable(x - dx, y + 1))) {
          return nodes[x][y];
        }
      } else if (dy != 0) {
        if ((isWalkable(x - 1, y) && !isWalkable(x - 1, y - dy)) ||
            (isWalkable(x + 1, y) && !isWalkable(x + 1, y - dy))) {
          return nodes[x][y];
        }
      }
    }

    if (isWalkable(x + dx, y) && isWalkable(x, y + dy)) {
      return jump(x + dx, y + dy, x, y, animated);
    } else {
      return null;
    }

  }

}

package search;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import util.Node;
import util.StatusManager;
import util.Util;

import java.util.ArrayList;

public abstract class JPS {

  protected Node[][] nodes;
  private Node start;
  protected Node end;
  private ArrayList<Node> closedList = new ArrayList<>();
  private ArrayList<Node> openList = new ArrayList<>();
  private int speed;
  private Timeline timeline;
  private float duration = 0f;
  private AnchorPane background;
  private String heuristics;
  private String diagonal;

  public JPS(Node[][] nodes, Node start, Node end, int speed, String diagonal, String heuristics, AnchorPane background) {

    this.nodes = nodes;
    this.start = start;
    this.end = end;
    this.speed = speed;
    this.heuristics = heuristics.toLowerCase();
    this.diagonal = diagonal;
    this.background = background;

  }


  // Start an animated search.
  public void start() {

    long startTime = System.nanoTime();

    openList.add(start);
    start.setParent(null);

    timeline = new Timeline(new KeyFrame(Duration.millis(10 - (speed - 1)), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        if (!openList.isEmpty()) {

          Node curNode = getNodeWithLowestFCost();
          openList.remove(curNode);
          closedList.add(curNode);
          if (curNode != start && curNode != end) {
            curNode.getNode().getStyleClass().add("closed");
          }

          if (curNode == end) {
            duration = (System.nanoTime() - startTime) / 1000000000f;
            timeline.stop();
            StatusManager.handleStatus("status", "Path has been found! Displaying results..");
            Util.retracePath(nodes, curNode, start, end, background, "JPS", heuristics, diagonal, speed, duration, openList, closedList);
          }

          identifySuccessors(curNode, true);

        } else {
          StatusManager.handleStatus("error", "The end node was not found!");
          timeline.stop();
        }

      }
    }));

    timeline.setCycleCount(Animation.INDEFINITE);
    timeline.play();

  }


  // Start a search without animation.
  public void startWithoutAnimation() {

    long startTime = System.nanoTime();
    boolean found = false;

    openList.add(start);
    start.setParent(null);

    while (!openList.isEmpty()) {

      Node curNode = getNodeWithLowestFCost();
      openList.remove(curNode);
      closedList.add(curNode);

      if (curNode == end) {
        found = true;
        duration = (System.nanoTime() - startTime) / 1000000000f;
        StatusManager.handleStatus("status", "Path has been found! Displaying results..");
        Util.retracePath(nodes, curNode, start, end, background, "JPS", heuristics, diagonal, 0,
            duration, openList, closedList);
      }

      identifySuccessors(curNode, false);

    }

    if (!found) {
      StatusManager.handleStatus("error", "The end node was not found!");
    }

  }


  // Get the node with the lowest f cost from the open list.
  private Node getNodeWithLowestFCost() {
    Node lowestNode = openList.get(0);

    for (Node n : openList) {
      if (n.getfCost() < lowestNode.getfCost() || n.getfCost() == lowestNode.getfCost() && n.gethCost() < lowestNode.gethCost()) {
        lowestNode = n;
      }
    }
    return lowestNode;
  }


  // Identify jump points, set their costs, and add them to the open list.
  private void identifySuccessors(Node node, boolean animated) {

    ArrayList<Node> neighbours = getNeighboursPruned(node);

    for (Node n : neighbours) {

      Node jumpPoint = jump(n.getX(), n.getY(), node.getX(), node.getY(), animated);

      if (jumpPoint != null) {

        if (closedList.contains(jumpPoint)) {
          continue;
        }

        double distance = Util.getDistance(jumpPoint, node, heuristics);
        double newCostToJumpPoint = node.getgCost() + distance;

        if (!openList.contains(jumpPoint) || newCostToJumpPoint < jumpPoint.getgCost()) {

          jumpPoint.setgCost(newCostToJumpPoint);
          jumpPoint.sethCost(Util.getDistance(jumpPoint, end, heuristics));
          jumpPoint.setParent(node);

          if (!openList.contains(jumpPoint)) {
            openList.add(jumpPoint);
            if (animated) {
              if (jumpPoint != start && jumpPoint != end) {
                jumpPoint.getNode().getStyleClass().add("open");
              }
            }
            // Util.animateNodeColoring(jumpPoint, start, end, speed);
          }

        }

      }

    }

  }


  // Gets the neighbours of a node according to the diagonal strategy.
  protected abstract ArrayList<Node> getNeighboursPruned(Node node);

  // Recursively searches for the next jump point according to the diagonal strategy.
  protected abstract Node jump(int x, int y, int px, int py, boolean animated);


  protected boolean isWalkable(int x, int y) {
    return x >= 0 && y >= 0 && x < nodes.length && y < nodes[0].length && !nodes[x][y].getType().equals("obstacle");
  }


  public Timeline getTimeline() {
    return timeline;
  }



}

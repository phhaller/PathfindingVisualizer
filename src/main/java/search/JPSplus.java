package search;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import util.Node;
import util.StatusManager;
import util.Util;

import java.util.ArrayList;
import java.util.HashMap;

public class JPSplus {

  private Node[][] nodes;
  private Node start;
  private Node end;
  private ArrayList<Node> closedList = new ArrayList<>();
  private ArrayList<Node> openList = new ArrayList<>();
  private int speed;
  private Timeline timeline;
  private float duration = 0f;
  private AnchorPane background;

  private HashMap<String, String[]> dirLookUpTable;
  private HashMap<String, int[]> dirIdxLookUp;
  private ArrayList<String> cardinalDirections;
  private ArrayList<String> diagonalDirections;

  public JPSplus(Node[][] nodes, Node start, Node end, int speed, AnchorPane background) {

    this.nodes = nodes;
    this.start = start;
    this.end = end;
    this.speed = speed;
    this.background = background;

    createUtility();
    precomputeMap();

  }


  // Create some utility HashMaps and ArrayLists to store directions.
  private void createUtility() {

    dirLookUpTable = new HashMap<>();
    dirLookUpTable.put("SOUTH", new String[]{"WEST", "SOUTHWEST", "SOUTH", "SOUTHEAST", "EAST"});
    dirLookUpTable.put("SOUTHEAST", new String[]{"SOUTH", "SOUTHEAST", "EAST"});
    dirLookUpTable.put("EAST", new String[]{"SOUTH", "SOUTHEAST", "EAST", "NORTHEAST", "NORTH"});
    dirLookUpTable.put("NORTHEAST", new String[]{"EAST", "NORTHEAST", "NORTH"});
    dirLookUpTable.put("NORTH", new String[]{"EAST", "NORTHEAST", "NORTH", "NORTHWEST", "WEST"});
    dirLookUpTable.put("NORTHWEST", new String[]{"NORTH", "NORTHWEST", "WEST"});
    dirLookUpTable.put("WEST", new String[]{"NORTH", "NORTHWEST", "WEST", "SOUTHWEST", "SOUTH"});
    dirLookUpTable.put("SOUTHWEST", new String[]{"WEST", "SOUTHWEST", "SOUTH"});
    dirLookUpTable.put("START", new String[]{"NORTH", "NORTHEAST", "EAST", "SOUTHEAST", "SOUTH", "SOUTHWEST",
        "WEST", "NORTHWEST"});

    dirIdxLookUp = new HashMap<>();
    dirIdxLookUp.put("NORTH", new int[]{0, -1});
    dirIdxLookUp.put("NORTHEAST", new int[]{1, -1});
    dirIdxLookUp.put("EAST", new int[]{1, 0});
    dirIdxLookUp.put("SOUTHEAST", new int[]{1, 1});
    dirIdxLookUp.put("SOUTH", new int[]{0, 1});
    dirIdxLookUp.put("SOUTHWEST", new int[]{-1, 1});
    dirIdxLookUp.put("WEST", new int[]{-1, 0});
    dirIdxLookUp.put("NORTHWEST", new int[]{-1, -1});

    cardinalDirections = new ArrayList<>();
    cardinalDirections.add("NORTH");
    cardinalDirections.add("EAST");
    cardinalDirections.add("SOUTH");
    cardinalDirections.add("WEST");

    diagonalDirections = new ArrayList<>();
    diagonalDirections.add("NORTHEAST");
    diagonalDirections.add("SOUTHEAST");
    diagonalDirections.add("SOUTHWEST");
    diagonalDirections.add("NORTHWEST");

  }


  // Initialize the precomputation process
  private void precomputeMap() {

    identifyPrimaryJumpPoints();
    sweepMap();

  }


  // Identify all primary jump points and set the values of those nodes accordingly.
  private void identifyPrimaryJumpPoints() {

    int width = nodes.length;
    int height = nodes[0].length;

    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        if (!nodes[x][y].getType().equals("obstacle")) {

          if (checkForForcedNeighbours(x, y, "EAST", width, height)) {
            nodes[x][y].setJumpPoint("EAST");
          }

          if (checkForForcedNeighbours(x, y, "WEST", width, height)) {
            nodes[x][y].setJumpPoint("WEST");
          }

          if (checkForForcedNeighbours(x, y, "NORTH", width, height)) {
            nodes[x][y].setJumpPoint("NORTH");
          }

          if (checkForForcedNeighbours(x, y, "SOUTH", width, height)) {
            nodes[x][y].setJumpPoint("SOUTH");
          }

        }

      }
    }

  }


  // Check if a node is a forced neighbour according to the direction of travel.
  private boolean checkForForcedNeighbours(int x, int y, String direction, int width, int height) {

    switch(direction) {

      case "EAST":
        if (x - 1 >= 0 &&
            y - 1 >= 0 &&
            !nodes[x-1][y].getType().equals("obstacle") &&
            nodes[x-1][y-1].getType().equals("obstacle") &&
            !nodes[x][y-1].getType().equals("obstacle")) {
          return true;
        }

        if (x - 1 >= 0 &&
            y + 1 < height &&
            !nodes[x-1][y].getType().equals("obstacle") &&
            nodes[x-1][y+1].getType().equals("obstacle") &&
            !nodes[x][y+1].getType().equals("obstacle")) {
          return true;
        }
        break;
      case "WEST":
        if (x + 1 < width &&
            y - 1 >= 0 &&
            !nodes[x+1][y].getType().equals("obstacle") &&
            nodes[x+1][y-1].getType().equals("obstacle") &&
            !nodes[x][y-1].getType().equals("obstacle")) {
          return true;
        }

        if (x + 1 < width &&
            y + 1 < height &&
            !nodes[x+1][y].getType().equals("obstacle") &&
            nodes[x+1][y+1].getType().equals("obstacle") &&
            !nodes[x][y+1].getType().equals("obstacle")) {
          return true;
        }
        break;
      case "NORTH":
        if (y + 1 < height &&
            x - 1 >= 0 &&
            !nodes[x][y+1].getType().equals("obstacle") &&
            nodes[x-1][y+1].getType().equals("obstacle") &&
            !nodes[x-1][y].getType().equals("obstacle")) {
          return true;
        }

        if (y + 1 < height &&
            x + 1 < width &&
            !nodes[x][y+1].getType().equals("obstacle") &&
            nodes[x+1][y+1].getType().equals("obstacle") &&
            !nodes[x+1][y].getType().equals("obstacle")) {
          return true;
        }
        break;
      case "SOUTH":
        if (y - 1 >= 0 &&
            x - 1 >= 0 &&
            !nodes[x][y-1].getType().equals("obstacle") &&
            nodes[x-1][y-1].getType().equals("obstacle") &&
            !nodes[x-1][y].getType().equals("obstacle")) {
          return true;
        }

        if (y - 1 >= 0 &&
            x + 1 < width &&
            !nodes[x][y-1].getType().equals("obstacle") &&
            nodes[x+1][y-1].getType().equals("obstacle") &&
            !nodes[x+1][y].getType().equals("obstacle")) {
          return true;
        }
        break;

    }


    return false;

  }


  // Sweep the grid and set the node values for distances to walls and jump points.
  private void sweepMap() {

    int width = nodes.length;
    int height = nodes[0].length;

    // NORTH
    for (int x = 0; x < width; ++x) {

      int count = -1;
      boolean jumpPointLastSeen = false;

      for (int y = 0; y < height; ++y) {

        if (nodes[x][y].getType().equals("obstacle")) {
          count = -1;
          jumpPointLastSeen = false;
          nodes[x][y].addDistanceToMap("NORTH", 0);
          continue;
        }

        count++;

        if (jumpPointLastSeen) {
          nodes[x][y].addDistanceToMap("NORTH", count);
        } else {
          nodes[x][y].addDistanceToMap("NORTH", -count);
        }

        if (nodes[x][y].isNorthJumpPoint()) {
          count = 0;
          jumpPointLastSeen = true;
        }

      }

    }


    // EAST
    for (int y = 0; y < height; ++y) {

      int count = -1;
      boolean jumpPointLastSeen = false;

      for (int x = width - 1; x >= 0; --x) {

        if (nodes[x][y].getType().equals("obstacle")) {
          count = -1;
          jumpPointLastSeen = false;
          nodes[x][y].addDistanceToMap("EAST", 0);
          continue;
        }

        count++;

        if (jumpPointLastSeen) {
          nodes[x][y].addDistanceToMap("EAST", count);
        } else {
          nodes[x][y].addDistanceToMap("EAST", -count);
        }

        if (nodes[x][y].isEastJumpPoint()) {
          count = 0;
          jumpPointLastSeen = true;
        }

      }

    }


    // SOUTH
    for (int x = 0; x < width; ++x) {

      int count = -1;
      boolean jumpPointLastSeen = false;

      for (int y = height - 1; y >= 0; --y) {

        if (nodes[x][y].getType().equals("obstacle")) {
          count = -1;
          jumpPointLastSeen = false;
          nodes[x][y].addDistanceToMap("SOUTH", 0);
          continue;
        }

        count++;

        if (jumpPointLastSeen) {
          nodes[x][y].addDistanceToMap("SOUTH", count);
        } else {
          nodes[x][y].addDistanceToMap("SOUTH", -count);
        }

        if (nodes[x][y].isSouthJumpPoint()) {
          count = 0;
          jumpPointLastSeen = true;
        }

      }

    }


    // WEST
    for (int y = 0; y < height; ++y) {

      int count = -1;
      boolean jumpPointLastSeen = false;

      for (int x = 0; x < width; ++x) {

        if (nodes[x][y].getType().equals("obstacle")) {
          count = -1;
          jumpPointLastSeen = false;
          nodes[x][y].addDistanceToMap("WEST", 0);
          continue;
        }

        count++;

        if (jumpPointLastSeen) {
          nodes[x][y].addDistanceToMap("WEST", count);
        } else {
          nodes[x][y].addDistanceToMap("WEST", -count);
        }

        if (nodes[x][y].isWestJumpPoint()) {
          count = 0;
          jumpPointLastSeen = true;
        }

      }

    }


    // NORTHEAST
    for (int x = width - 1; x >= 0; --x) {
      for (int y = 0; y < height; ++y) {

        if (!nodes[x][y].getType().equals("obstacle")) {
          if (y == 0 ||
              x == width - 1 ||
              nodes[x][y - 1].getType().equals("obstacle") ||
              nodes[x + 1][y].getType().equals("obstacle") ||
              nodes[x + 1][y - 1].getType().equals("obstacle")) {
            nodes[x][y].addDistanceToMap("NORTHEAST", 0);
          } else if (!nodes[x][y - 1].getType().equals("obstacle") &&
              !nodes[x + 1][y].getType().equals("obstacle") &&
              (nodes[x + 1][y - 1].getDistanceFromMap("NORTH") > 0 ||
                  nodes[x + 1][y - 1].getDistanceFromMap("EAST") > 0)) {
            nodes[x][y].addDistanceToMap("NORTHEAST", 1);
          } else {
            int jumpDist = nodes[x + 1][y - 1].getDistanceFromMap("NORTHEAST");
            if (jumpDist > 0) {
              nodes[x][y].addDistanceToMap("NORTHEAST", 1 + jumpDist);
            } else {
              nodes[x][y].addDistanceToMap("NORTHEAST", -1 + jumpDist);
            }
          }
        }

      }
    }


    // NORTHWEST
    for (int x = 0; x < width; ++x) {
      for (int y = 0; y < height; ++y) {

        if (!nodes[x][y].getType().equals("obstacle")) {
          if (y == 0 ||
              x == 0 ||
              nodes[x][y - 1].getType().equals("obstacle") ||
              nodes[x - 1][y].getType().equals("obstacle") ||
              nodes[x - 1][y - 1].getType().equals("obstacle")) {
            nodes[x][y].addDistanceToMap("NORTHWEST", 0);
          } else if (!nodes[x][y - 1].getType().equals("obstacle") &&
              !nodes[x - 1][y].getType().equals("obstacle") &&
              (nodes[x - 1][y - 1].getDistanceFromMap("NORTH") > 0 ||
                  nodes[x - 1][y - 1].getDistanceFromMap("WEST") > 0)) {
            nodes[x][y].addDistanceToMap("NORTHWEST", 1);
          } else {
            int jumpDist = nodes[x - 1][y - 1].getDistanceFromMap("NORTHWEST");
            if (jumpDist > 0) {
              nodes[x][y].addDistanceToMap("NORTHWEST", 1 + jumpDist);
            } else {
              nodes[x][y].addDistanceToMap("NORTHWEST", -1 + jumpDist);
            }
          }
        }

      }
    }


    // SOUTHEAST
    for (int x = width - 1; x >= 0; --x) {
      for (int y = height - 1; y >= 0; --y) {

        if (!nodes[x][y].getType().equals("obstacle")) {
          if (y == height - 1 ||
              x == width - 1 ||
              nodes[x][y + 1].getType().equals("obstacle") ||
              nodes[x + 1][y].getType().equals("obstacle") ||
              nodes[x + 1][y + 1].getType().equals("obstacle")) {
            nodes[x][y].addDistanceToMap("SOUTHEAST", 0);
          } else if (!nodes[x][y + 1].getType().equals("obstacle") &&
              !nodes[x + 1][y].getType().equals("obstacle") &&
              (nodes[x + 1][y + 1].getDistanceFromMap("SOUTH") > 0 ||
                  nodes[x + 1][y + 1].getDistanceFromMap("EAST") > 0)) {
            nodes[x][y].addDistanceToMap("SOUTHEAST", 1);
          } else {
            int jumpDist = nodes[x + 1][y + 1].getDistanceFromMap("SOUTHEAST");
            if (jumpDist > 0) {
              nodes[x][y].addDistanceToMap("SOUTHEAST", 1 + jumpDist);
            } else {
              nodes[x][y].addDistanceToMap("SOUTHEAST", -1 + jumpDist);
            }
          }
        }

      }
    }


    // SOUTHWEST
    for (int x = 0; x < width; ++x) {
      for (int y = height - 1; y >= 0; --y) {

        if (!nodes[x][y].getType().equals("obstacle")) {
          if (y == height - 1 ||
              x == 0 ||
              nodes[x][y + 1].getType().equals("obstacle") ||
              nodes[x - 1][y].getType().equals("obstacle") ||
              nodes[x - 1][y + 1].getType().equals("obstacle")) {
            nodes[x][y].addDistanceToMap("SOUTHWEST", 0);
          } else if (!nodes[x][y + 1].getType().equals("obstacle") &&
              !nodes[x - 1][y].getType().equals("obstacle") &&
              (nodes[x - 1][y + 1].getDistanceFromMap("SOUTH") > 0 ||
                  nodes[x - 1][y + 1].getDistanceFromMap("WEST") > 0)) {
            nodes[x][y].addDistanceToMap("SOUTHWEST", 1);
          } else {
            int jumpDist = nodes[x - 1][y + 1].getDistanceFromMap("SOUTHWEST");
            if (jumpDist > 0) {
              nodes[x][y].addDistanceToMap("SOUTHWEST", 1 + jumpDist);
            } else {
              nodes[x][y].addDistanceToMap("SOUTHWEST", -1 + jumpDist);
            }
          }
        }

      }
    }

  }


  // Start an animated search.
  public void start() {

    long startTime = System.nanoTime();

    openList.add(start);
    start.setParent(null);

    timeline = new Timeline(new KeyFrame(Duration.millis(10 - (speed - 1)), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        if (!openList.isEmpty()) {

          Node curNode = getNodeWithLowestFCost();
          Node parent = curNode.getParent();
          openList.remove(curNode);
          closedList.add(curNode);
          if (curNode != start && curNode != end) {
            curNode.getNode().getStyleClass().add("closed");
          }

          if (curNode == end) {
            StatusManager.handleStatus("status", "Path has been found! Displaying results..");
            duration = (System.nanoTime() - startTime) / 1000000000f;
            timeline.stop();
            Util.retracePath(nodes, curNode, start, end, background, "JPS+", "Octile",
                "No_obstacles", speed, duration, openList, closedList);
            resetNodes();
          }

          String travelDir = "";

          if (parent == null) {
            travelDir = "START";
          } else {
            travelDir = getTravelDir(curNode, parent);
          }

          for (String direction : dirLookUpTable.get(travelDir)) {

            Node successor = null;
            double givenCost = 0f;

            if (cardinalDirections.contains(direction) &&
                goalInExactDirection(curNode, direction) &&
                diffNodes(curNode, end) <= Math.abs(curNode.getDistanceFromMap(direction))) {
              successor = end;
              givenCost = curNode.getgCost() + diffNodes(curNode, end);
            } else if (diagonalDirections.contains(direction) &&
                goalInGeneralDirection(curNode, direction) &&
                (diffNodesRow(curNode, end) <= Math.abs(curNode.getDistanceFromMap(direction)) ||
                    diffNodesCol(curNode, end) <= Math.abs(curNode.getDistanceFromMap(direction)))) {
              int minDiff = Math.min(diffNodesRow(curNode, end), diffNodesCol(curNode, end));
              successor = getNode(curNode, minDiff, direction);
              givenCost = curNode.getgCost() + (Math.sqrt(2) * minDiff);
            } else if (curNode.getDistanceFromMap(direction) > 0) {
              successor = getNode(curNode, direction);
              givenCost = diffNodes(curNode, successor);
              if (diagonalDirections.contains(direction)) {
                givenCost *= Math.sqrt(2);
              }
              givenCost += curNode.getgCost();
            }


            if (successor != null) {
              if (!openList.contains(successor) && !closedList.contains(successor)) {
                successor.setParent(curNode);
                successor.setgCost(givenCost);
                successor.sethCost(getHeuristics(successor, end));
                openList.add(successor);
                if (successor != start && successor != end) {
                  successor.getNode().getStyleClass().add("open");
                  // Util.animateNodeColoring(successor, start, end, speed);
                }
              } else if (givenCost < successor.getgCost()) {
                successor.setParent(curNode);
                successor.setgCost(givenCost);
                successor.sethCost(getHeuristics(successor, end));
                if (!openList.contains(successor)) {
                  openList.add(successor);
                }
              }
            }

          }

        } else {
          timeline.stop();
          StatusManager.handleStatus("error", "The end node was not found!");
        }

      }
    }));

    timeline.setCycleCount(Animation.INDEFINITE);
    timeline.play();

  }


  // Start a search without animation.
  public void startWithoutAnimation() {

    long startTime = System.nanoTime();
    boolean found = false;

    openList.add(start);
    start.setParent(null);

    while (!openList.isEmpty()) {

      Node curNode = getNodeWithLowestFCost();
      Node parent = curNode.getParent();
      openList.remove(curNode);
      closedList.add(curNode);

      if (curNode == end) {
        found = true;
        duration = (System.nanoTime() - startTime) / 1000000000f;
        StatusManager.handleStatus("status", "Path has been found! Displaying results..");
        Util.retracePath(nodes, curNode, start, end, background, "JPS+", "Octile",
            "No_obstacles", speed, duration, openList, closedList);
        resetNodes();
      }

      String travelDir = "";

      if (parent == null) {
        travelDir = "START";
      } else {
        travelDir = getTravelDir(curNode, parent);
      }

      for (String direction : dirLookUpTable.get(travelDir)) {

        Node successor = null;
        double givenCost = 0f;

        if (cardinalDirections.contains(direction) &&
            goalInExactDirection(curNode, direction) &&
            diffNodes(curNode, end) <= Math.abs(curNode.getDistanceFromMap(direction))) {
          successor = end;
          givenCost = curNode.getgCost() + diffNodes(curNode, end);
        } else if (diagonalDirections.contains(direction) &&
            goalInGeneralDirection(curNode, direction) &&
            (diffNodesRow(curNode, end) <= Math.abs(curNode.getDistanceFromMap(direction)) ||
                diffNodesCol(curNode, end) <= Math.abs(curNode.getDistanceFromMap(direction)))) {
          int minDiff = Math.min(diffNodesRow(curNode, end), diffNodesCol(curNode, end));
          successor = getNode(curNode, minDiff, direction);
          givenCost = curNode.getgCost() + (Math.sqrt(2) * minDiff);
        } else if (curNode.getDistanceFromMap(direction) > 0) {
          successor = getNode(curNode, direction);
          givenCost = diffNodes(curNode, successor);
          if (diagonalDirections.contains(direction)) {
            givenCost *= Math.sqrt(2);
          }
          givenCost += curNode.getgCost();
        }


        if (successor != null) {
          if (!openList.contains(successor) && !closedList.contains(successor)) {
            successor.setParent(curNode);
            successor.setgCost(givenCost);
            successor.sethCost(getHeuristics(successor, end));
            openList.add(successor);
          } else if (givenCost < successor.getgCost()) {
            successor.setParent(curNode);
            successor.setgCost(givenCost);
            successor.sethCost(getHeuristics(successor, end));
            if (!openList.contains(successor)) {
              openList.add(successor);
            }
          }
        }

      }

    }

    if (!found) {
      StatusManager.handleStatus("error", "The end node was not found!");
    }

  }


  // Get the node with the lowest f cost from the open list.
  private Node getNodeWithLowestFCost() {
    Node lowestNode = openList.get(0);

    for (Node n : openList) {
      if (n.getfCost() < lowestNode.getfCost() || n.getfCost() == lowestNode.getfCost() && n.gethCost() < lowestNode.gethCost()) {
        lowestNode = n;
      }
    }
    return lowestNode;
  }


  // Get the direction of travel from a parents node to itself.
  private String getTravelDir(Node node, Node parent) {

    int x = node.getX();
    int y = node.getY();
    int px = parent.getX();
    int py = parent.getY();

    int dx = (x - px) / Math.max(Math.abs(x - px), 1);
    int dy = (y - py) / Math.max(Math.abs(y - py), 1);

    return getDirByIdx(dx, dy);

  }


  // Get the distance as String according the the differences in x and y directions.
  private String getDirByIdx(int dx, int dy) {

    if (dx == 0 && dy == -1) {
      return "NORTH";
    }

    if (dx == 1 && dy == -1) {
      return "NORTHEAST";
    }

    if (dx == 1 && dy == 0) {
      return "EAST";
    }

    if (dx == 1 && dy == 1) {
      return "SOUTHEAST";
    }

    if (dx == 0 && dy == 1) {
      return "SOUTH";
    }

    if (dx == -1 && dy == 1) {
      return "SOUTHWEST";
    }

    if (dx == -1 && dy == 0) {
      return "WEST";
    }

    if (dx == -1 && dy == -1) {
      return "NORTHWEST";
    }

    return "UNKNOWN";

  }


  // Get the distance between two nodes with the octile heuristic.
  private double getHeuristics(Node a, Node b) {
    int dx = Math.abs(b.getX() - a.getX());
    int dy = Math.abs(b.getY() - a.getY());
    return dx + dy + (Math.sqrt(2) - 2) * Math.min(dx, dy);
  }


  // Check if the end node is in the direction of travel.
  private boolean goalInExactDirection(Node node, String dir) {

    int[] delta = dirIdxLookUp.get(dir);
    int[] nodeIdx = new int[]{node.getX(), node.getY()};
    int[] goalIdx = new int[]{end.getX(), end.getY()};
    int idxToCheck = delta[0] == 0 ? 0 : 1;
    int dirToCheck = idxToCheck == 0 ? 1 : 0;

    if (nodeIdx[idxToCheck] == goalIdx[idxToCheck]) {
      if (delta[dirToCheck] > 0 && goalIdx[dirToCheck] > nodeIdx[dirToCheck]) {
        return true;
      }

      if (delta[dirToCheck] < 0 && goalIdx[dirToCheck] < nodeIdx[dirToCheck] ) {
        return true;
      }
    }

    return false;
  }


  // Check if the goal is in general direction of travel in case of traveling diagonally.
  private boolean goalInGeneralDirection(Node node, String dir) {

    int[] delta = dirIdxLookUp.get(dir);
    int[] nodeIdx = new int[]{node.getX(), node.getY()};
    int[] goalIdx = new int[]{end.getX(), end.getY()};

    return (delta[0]*(goalIdx[0] - nodeIdx[0]) > 0 && delta[1]*(goalIdx[1] - nodeIdx[1]) > 0);
  }


  // Get the difference between two nodes.
  private int diffNodes(Node a, Node b) {
    int dx = b.getX() - a.getX();
    int dy = b.getY() - a.getY();
    if (dx == 0 || dy == 0) {
      return Math.abs(dx) + Math.abs(dy);
    } else {
      return (Math.abs(dx) + Math.abs(dy)) / 2;
    }
  }


  // Get the difference between two nodes in the same row.
  private int diffNodesRow(Node a, Node b) {
    int dy = b.getY() - a.getY();
    return Math.abs(dy);
  }


  // Get the difference between two nodes in the sam column.
  private int diffNodesCol(Node a, Node b) {
    int dx = b.getX() - a.getX();
    return Math.abs(dx);
  }


  private Node getNode(Node node, String direction) {
    return getNode(node, 1000000000, direction);
  }


  // Get the next jump point according to the direction of travel.
  private Node getNode(Node node, int minDiff, String direction) {
    if (minDiff == 1000000000) {
      int[] delta = dirIdxLookUp.get(direction);
      int jumpDistance = node.getDistanceFromMap(direction);
      return nodes[node.getX() + delta[0] * jumpDistance][node.getY() + delta[1] * jumpDistance];
    } else {
      int[] delta = dirIdxLookUp.get(direction);
      return nodes[node.getX() + delta[0] * minDiff][node.getY() + delta[1] * minDiff];
    }
  }


  // Reset jump point and distance values of all nodes after a search.
  private void resetNodes() {
    for (int x = 0; x < nodes.length; x++) {
      for (int y = 0; y < nodes[0].length; y++) {
        nodes[x][y].resetNode();
      }
    }
  }


  public Timeline getTimeline() {
    return timeline;
  }

}

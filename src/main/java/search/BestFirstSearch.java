package search;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import util.Node;
import util.StatusManager;
import util.Util;

import java.util.LinkedList;

public class BestFirstSearch {

  private Node[][] nodes;
  private Node start;
  private Node end;
  private LinkedList<Node> closedList = new LinkedList<>();
  private LinkedList<Node> openList = new LinkedList<>();
  private int speed;
  private Timeline timeline;
  private float duration = 0f;
  private AnchorPane background;
  private String heuristics;
  private String diagonal;


  public BestFirstSearch(Node[][] nodes, Node start, Node end, int speed, String diagonal,
                         String heuristics, AnchorPane background) {
    this.nodes = nodes;
    this.start = start;
    this.end = end;
    this.speed = speed;
    this.diagonal = diagonal.toUpperCase();
    this.heuristics = heuristics.toLowerCase();
    this.background = background;
  }


  // Start an animated search.
  public void start() {

    long startTime = System.nanoTime();

    openList.add(start);
    start.setParent(null);

    timeline = new Timeline(new KeyFrame(Duration.millis(10 - (speed - 1)), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        if (!openList.isEmpty()) {

          Node curNode = getNodeWithLowestHCost();

          openList.remove(curNode);
          closedList.add(curNode);
          if (curNode != start && curNode != end) {
            curNode.getNode().getStyleClass().add("closed");
          }

          if (curNode == end) {
            duration = (System.nanoTime() - startTime) / 1000000000f;
            StatusManager.handleStatus("status", "Path has been found! Displaying results..");
            Util.constructPath(end, start, end, background, "Greedy BFS", heuristics, diagonal, speed, duration,
                openList, closedList);
            timeline.stop();
          }

          for (Node neighbour : Util.getNeighbours(nodes, curNode, diagonal)) {

            if (closedList.contains(neighbour) || neighbour.getType().equals("obstacle")) {
              continue;
            }

            double newHCost = Util.getDistance(neighbour, end, heuristics);
            if (newHCost < neighbour.gethCost() || !openList.contains(neighbour)) {

              neighbour.sethCost(newHCost);
              neighbour.setParent(curNode);

              if (!openList.contains(neighbour)) {
                openList.add(neighbour);
                if (neighbour != start && neighbour != end) {
                  neighbour.getNode().getStyleClass().add("open");
                }
                // Util.animateNodeColoring(neighbour, start, end, speed);
              }

            }

          }

        } else {
          StatusManager.handleStatus("error", "The end node was not found!");
          timeline.stop();
        }

      }
    }));

    timeline.setCycleCount(Animation.INDEFINITE);
    timeline.play();

  }


  // Start a search without animation.
  public void startWithoutAnimation() {

    long startTime = System.nanoTime();
    boolean found = false;

    openList.add(start);
    start.setParent(null);

    while (!openList.isEmpty()) {

      Node curNode = getNodeWithLowestHCost();

      openList.remove(curNode);
      closedList.add(curNode);

      if (curNode == end) {
        found = true;
        duration = (System.nanoTime() - startTime) / 1000000000f;
        StatusManager.handleStatus("status", "Path has been found! Displaying results..");
        Util.constructPath(end, start, end, background, "Greedy BFS", heuristics, diagonal, 0, duration,
            openList, closedList);
      }

      for (Node neighbour : Util.getNeighbours(nodes, curNode, diagonal)) {

        if (closedList.contains(neighbour) || neighbour.getType().equals("obstacle")) {
          continue;
        }

        double newHCost = Util.getDistance(neighbour, end, heuristics);
        if (newHCost < neighbour.gethCost() || !openList.contains(neighbour)) {

          neighbour.sethCost(newHCost);
          neighbour.setParent(curNode);

          if (!openList.contains(neighbour)) {
            openList.add(neighbour);
          }

        }

      }

    }

    if (!found) {
      StatusManager.handleStatus("error", "The end node was not found!");
    }

  }


  // Get the node with the lowest h cost from the open list.
  private Node getNodeWithLowestHCost() {
    Node lowestNode = openList.get(0);

    for (Node n : openList) {
      if (n.gethCost() < lowestNode.gethCost()) {
        lowestNode = n;
      }
    }
    return lowestNode;
  }


  public Timeline getTimeline() {
    return timeline;
  }

}

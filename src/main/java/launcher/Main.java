package launcher;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import view.MainWindowController;

import java.io.IOException;

public class Main extends Application {

  private Stage primaryStage;

  @Override
  public void start(Stage primaryStage) throws Exception {

    this.primaryStage = primaryStage;
    this.primaryStage.setTitle("PathFinder");
    this.primaryStage.setResizable(false);

    createWindow();

  }


  // Create the PathFinder window with the fxml file and set the fxml controller.
  private void createWindow() {

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/mainWindow.fxml"));
      AnchorPane rootPane = loader.load();
      Scene scene = new Scene(rootPane);

      MainWindowController controller = loader.getController();

      primaryStage.setScene(scene);
      primaryStage.show();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }


  public static void main(String[] args) {
    launch(args);
  }


}

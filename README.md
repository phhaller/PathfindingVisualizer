# PathfindingVisualizer




## Description
This project aims to explore some of the most popular algorithms used for pathfinding on grid-based maps and to provide 
a framework to display the algorithms' path finding process visually.

Currently, following algorithms are implemented:
- Breadth First Search
- Dijkstra
- Greedy Best First Search
- A*
- Jump Point Search
- Jump Point Search Plus


## Installation
The project is set up to run on Java 11 in a MacOS or Windows environment. To change the used Java version simply edit 
the corresponding line (sourceCompatibility) in the build.gradle file and rebuild the project. 
To run the project in a Linux environment, simply add the missing dependencies in the build.gradle file and rebuild the 
project.


## Usage
The project can be executed by running the class Launcher located in the launcher package in an IDE, 
or by running the executable jar file located at the /build/libs folder (java -jar PathfindingVisualizer-1.jar).

Once you successfully started the project you can:

- General:
    - Click and drag the start (Green) and end node (Red) to move them around the grid.
    - Click and/or drag anywhere on the grid to create or remove obstacles.
    
- Menu:
    - Click the "i" button to show help menu.
    - Create an empty map, a preloaded map, or a random maze.
    - Choose one of the available pathfinding algorithms.
    - If available, choose a heuristic.
    - If available, choose the way in which the search is allowed to move and move along or through diagonal obstacles.
    - Enable or disable the animation. If enabled, choose the desired animation speed.
    - Click the Start button to start a new search, or restart a currently paused one.
    - Click the Stop button to stop a currently running search.
    - Click the Clear button to clear the current search. If the search is already cleared, all obstacles are removed and start and end nodes reset.
    
Let's get searchin!


## Roadmap
This project can be considered finished. There are some minor things that I'm not completely happy with and that could be changed, 
but for now I'll leave it the way it is.

Next to the search algorithms themselves, I'd also be interested to dig deeper into map abstractions to bring down the searches
computational cost even further ([example](https://webdocs.cs.ualberta.ca/~nathanst/papers/mmabstraction.pdf)). 
This is an addition that might happen in the future.